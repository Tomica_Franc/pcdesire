package pc.desire.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pc.desire.app.model.Computer;
import pc.desire.app.repository.ComputerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ComputerService {

    @Autowired
    ComputerRepository computerRepository;

    public List<Computer> getAll() {
        return computerRepository.findAll();
    }

    public Optional<Computer> findById(Integer id) {
        return computerRepository.findById(id);
    }

    public Computer getComputerByName(String name) {
        return computerRepository.findByName(name);
    }

    public void createComputer(Computer computer) {
        computerRepository.save(computer);
    }

    public Computer getComputer(Integer id) {
        return computerRepository.getOne(id);
    }

    public void deleteComputer(Computer computer) {
        computerRepository.delete(computer);
    }

    public List<Computer> getComputersByComputerIds(List<Integer> computerIds) {
        return computerRepository.findByIdIn(computerIds);
    }
}
