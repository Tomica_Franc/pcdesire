package pc.desire.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pc.desire.app.model.CompUser;
import pc.desire.app.repository.CompUserRepository;

import java.util.List;

@Service
public class CompUserService {

    @Autowired
    CompUserRepository compUserRepository;

    public List<CompUser> getCompUserByUserId(Integer userId) {
        return compUserRepository.findAllByUserid(userId);
    }

    public List<CompUser> getCompUserByPcId(Integer pcId) {
        return compUserRepository.findAllByComputerid(pcId);
    }

    public void createCompUser(CompUser compUser) {
        compUserRepository.save(compUser);
    }

    public CompUser getCompUserByUserIdAndPcId(Integer userid, Integer pcid) {
        return compUserRepository.findByUseridAndComputerid(userid, pcid);
    }

    public void deleteCompUser(CompUser compUser) {
        compUserRepository.delete(compUser);
    }
}
