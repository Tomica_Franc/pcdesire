package pc.desire.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pc.desire.app.model.Component;
import pc.desire.app.repository.ComponentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ComponentService {

    @Autowired
    ComponentRepository componentRepository;

    public List<Component> getAll() {
        return componentRepository.findAll();
    }

    public Optional<Component> findById(Integer id) {
        return componentRepository.findById(id);
    }

    public Component getComponentByName(String name) {
        return componentRepository.findByName(name);
    }

    public List<Component> getComponentByComponentType(String componenttype) {
        return componentRepository.findAllByComponenttype(componenttype);
    }

    public void createComponent(Component component) {
        componentRepository.save(component);
    }

    public Component getComponent(Integer id) {
        return componentRepository.getOne(id);
    }

    public void deleteComponent(Component component) {
        componentRepository.delete(component);
    }
}
