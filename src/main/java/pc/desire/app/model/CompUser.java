package pc.desire.app.model;

import javax.persistence.*;

@Entity
@Table(name = "compuser", catalog = "pcdesire")
public class CompUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "userid")
    private int userid;

    @Column(name = "computerid")
    private int computerid;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getComputerid() {
        return computerid;
    }

    public void setComputerid(int computerid) {
        this.computerid = computerid;
    }
}
