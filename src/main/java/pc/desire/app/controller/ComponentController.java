package pc.desire.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pc.desire.app.model.Component;
import pc.desire.app.service.ComponentService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/component")
public class ComponentController {

    @Autowired
    ComponentService componentService;

    @GetMapping("/getAll")
    public List<Component> getAllComponents() {
        return componentService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Component getComponentById(@PathVariable final Integer id) {
        Optional<Component> optional = componentService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No Component found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public Component getComponentByName(@PathVariable final String name) {
        return componentService.getComponentByName(name);
    }

    @GetMapping("/getByComponentType/{componentType}")
    public List<Component> getComponentByComponentType(@PathVariable final String componentType) {
        return componentService.getComponentByComponentType(componentType);
    }

    @PostMapping("/createComponent")
    public void createComponent(@RequestBody Component component) {
        if(component.getName() == null || component.getComponenttype() == null) {
            throw new RuntimeException("Component does not have all arguments!");
        } else if(componentService.getComponentByName(component.getName()) != null) {
            throw new RuntimeException("Component with same name already exists!");
        }

        if(!validateComponentType(component.getComponenttype())) {
            throw new RuntimeException("Component type is not valid!");
        } else {
            componentService.createComponent(component);
        }
    }

    @PutMapping("/updateComponent/{id}")
    public void updateComponent(@RequestBody Component component, @PathVariable int id) {

        Optional<Component> optional = componentService.findById(id);
        Component currentComponent = optional.get();

        if (currentComponent != null) {
            if(component.getName() != null)
                if(componentService.getComponentByName(component.getName()) != null) {
                    throw new RuntimeException("Component with same name already exists!");
                }
                currentComponent.setName(component.getName());
            if(component.getComponenttype() != null) {
                if(!validateComponentType(component.getComponenttype())) {
                    throw new RuntimeException("Component type is not valid!");
                } else {
                    currentComponent.setComponenttype(component.getComponenttype());
                }
            }

            componentService.createComponent(currentComponent);
        } else {
            throw new RuntimeException("Component does not exist!");
        }
    }

    @GetMapping("/deleteComponent/{id}")
    public void deleteComponent(@PathVariable final Integer id) {
        Component component = componentService.getComponent(id);

        if(component != null) {
            componentService.deleteComponent(component);
        }
    }

    /**
     * Validating component type
     * Component type must be CPU, GPU, HDD or RAM
     * @param componenttype
     * @return
     */
    private boolean validateComponentType(String componenttype) {
        if(!componenttype.equals("GPU") || !componenttype.equals("CPU") || !componenttype.equals("HDD") || !componenttype.equals("RAM")) {
            return false;
        }
        return true;
    }

}
