package pc.desire.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pc.desire.app.model.CompUser;
import pc.desire.app.service.CompUserService;
import pc.desire.app.service.ComputerService;
import pc.desire.app.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/compuser")
public class CompUserController {

    @Autowired
    CompUserService compUserService;

    @Autowired
    UserService userService;

    @Autowired
    ComputerService computerService;

    @GetMapping("/getByUserId/{userId}")
    public List<CompUser> getCompUsersByUserId(@PathVariable final Integer userId) {
        return compUserService.getCompUserByUserId(userId);
    }

    @GetMapping("/getByPcId/{pcId}")
    public List<CompUser> getCompUsersByPcId(@PathVariable final Integer pcId) {
        return compUserService.getCompUserByPcId(pcId);
    }

    @GetMapping("/addInterest/{userid}/{pcid}")
    public void addInterest(@PathVariable final Integer userid, @PathVariable final Integer pcid) {
        CompUser compUser = new CompUser();

        if(userid == 0 || pcid == 0) {
            throw new RuntimeException("CompUser does not have all parameters!");
        }
        if(userService.getUser(userid) == null) {
            throw new RuntimeException("User does not exist!");
        }
        if(computerService.getComputer(pcid) == null) {
            throw new RuntimeException("Computer does not exist!");
        }

        compUser.setUserid(userid);
        compUser.setComputerid(pcid);

        compUserService.createCompUser(compUser);
    }

    @GetMapping("/deleteInterest/{userid}/{pcid}")
    public void deleteInterest(@PathVariable final Integer userid, @PathVariable final Integer pcid) {
        CompUser compUser = compUserService.getCompUserByUserIdAndPcId(userid, pcid);

        if(compUser != null) {
            compUserService.deleteCompUser(compUser);
        }
    }
}
