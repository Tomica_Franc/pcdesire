package pc.desire.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pc.desire.app.dto.ComputerDTO;
import pc.desire.app.model.*;
import pc.desire.app.service.CompUserService;
import pc.desire.app.service.ComponentService;
import pc.desire.app.service.ComputerService;
import pc.desire.app.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/computer")
public class ComputerController {

    @Autowired
    ComputerService computerService;

    @Autowired
    ComponentService componentService;

    @Autowired
    CompUserService compUserService;

    @Autowired
    UserService userService;

    @GetMapping("/getAll")
    public List<ComputerDTO> getAllComputers() {
        List<Computer> computers =  computerService.getAll();

        List<ComputerDTO> computerDTOS = convertEntityToDTO(computers);

        return computerDTOS;
    }

    @GetMapping("/getById/{id}")
    public ComputerDTO getComputerById(@PathVariable final Integer id) {
        Optional<Computer> optional = computerService.findById(id);
        Computer pc = optional.get();

        ComputerDTO computerDTO = returnDto(optional.get());

        return computerDTO;
    }

    @GetMapping("/getByName/{name}")
    public ComputerDTO getComputerByName(@PathVariable final String name) {
        Computer pc = computerService.getComputerByName(name);

        ComputerDTO computerDTO = returnDto(pc);

        return computerDTO;
    }

    @PostMapping("/createComputer")
    public void createComputer(@RequestBody Computer computer) {
        if(computer.getName() == null || computer.getGpu() == 0 || computer.getCpu() == 0 || computer.getHdd() == 0 || computer.getRam() == 0) {
                throw new RuntimeException("Computer does not have all arguments!");
        } else if(computerService.getComputerByName(computer.getName()) != null) {
            throw new RuntimeException("Computer with same name already exists!");
        }

        if(!validateComputer(computer)) {
            throw new RuntimeException("Some components does not have right type!");
        }

        computerService.createComputer(computer);
    }

    /**
     * Creating computer for specific user
     * @param computer
     * @param userid
     */
    @PostMapping("/createComputer/{userid}")
    public void createComputer(@RequestBody Computer computer, @PathVariable final Integer userid) {
        if(computer.getName() == null || computer.getGpu() == 0 || computer.getCpu() == 0 || computer.getHdd() == 0 || computer.getRam() == 0) {
            throw new RuntimeException("Computer does not have all arguments!");
        } else if(computerService.getComputerByName(computer.getName()) != null) {
            throw new RuntimeException("Computer with same name already exists!");
        } else if(userService.getUser(userid) == null){
            throw new RuntimeException("User does not exist!");
        }

        if(!validateComputer(computer)) {
            throw new RuntimeException("Some components does not have right type!");
        }

        computerService.createComputer(computer);

        CompUser compUser = new CompUser();
        Computer comp = computerService.getComputerByName(computer.getName());
        if(comp == null) {
            throw new RuntimeException("Computer with given name does not exist!");
        }
        compUser.setComputerid(comp.getId());
        compUser.setUserid(userid);
        compUserService.createCompUser(compUser);
    }

    @PutMapping("/updateComputer/{id}")
    public void updateComputer(@RequestBody Computer computer, @PathVariable int id) {

        Optional<Computer> optional = computerService.findById(id);
        Computer currentComputer = optional.get();

        if (currentComputer != null) {
            if(computer.getName() != null) {
                if(computerService.getComputerByName(computer.getName()) != null) {
                    throw new RuntimeException("Computer with same name already exists!");
                }
                currentComputer.setName(computer.getName());
            }
            if(computer.getGpu() != 0) {
                currentComputer.setGpu(computer.getGpu());
            }
            if(computer.getCpu() != 0) {
                currentComputer.setCpu(computer.getCpu());
            }
            if(computer.getHdd() != 0) {
                currentComputer.setHdd(computer.getHdd());
            }
            if(computer.getRam() != 0) {
                currentComputer.setRam(computer.getRam());
            }

            if(!validateComputer(computer)) {
                throw new RuntimeException("Some components does not have right type!");
            }

            computerService.createComputer(currentComputer);
        } else {
            throw new RuntimeException("Computer does not exist!");
        }
    }

    @GetMapping("/deleteComputer/{id}")
    public void deleteComputer(@PathVariable final Integer id) {
        Computer computer = computerService.getComputer(id);

        if(computer != null) {
            computerService.deleteComputer(computer);
        }
    }

    @GetMapping("/getAllComputersForUser/{userid}")
    public List<ComputerDTO> findAllComputersForUser(@PathVariable final Integer userid) {
        List<CompUser> compUsers = compUserService.getCompUserByUserId(userid);

        List<Integer> computerIds = compUsers.
                stream().
                map(CompUser::getComputerid).
                collect(Collectors.toList());

        List<Computer> computers = computerService.getComputersByComputerIds(computerIds);

        List<ComputerDTO> computerDTOS = convertEntityToDTO(computers);

        return computerDTOS;
    }

    /**
     * Converts entity to dto
     * @param computers
     * @return
     */
    private List<ComputerDTO> convertEntityToDTO(List<Computer> computers) {
        List<ComputerDTO> computerDTOS = new ArrayList<>();

        for(Computer pc : computers) {
            ComputerDTO computerDTO = new ComputerDTO();

            if(pc.getId() != 0) {
                computerDTO.setId(pc.getId());
            }

            if(pc.getName() != null) {
                computerDTO.setName(pc.getName());
            }

            if(pc.getGpu() != 0) {
                Component component = componentService.getComponent(pc.getGpu());
                if(component != null) {
                    if(component.getName() != null) {
                        computerDTO.setGpuName(component.getName());
                    }
                }
            }
            if(pc.getCpu() != 0) {
                Component component = componentService.getComponent(pc.getCpu());
                if(component != null) {
                    if(component.getName() != null) {
                        computerDTO.setCpuName(component.getName());
                    }
                }
            }
            if(pc.getHdd() != 0) {
                Component component = componentService.getComponent(pc.getHdd());
                if(component != null) {
                    if(component.getName() != null) {
                        computerDTO.setHddName(component.getName());
                    }
                }
            }
            if(pc.getRam() != 0) {
                Component component = componentService.getComponent(pc.getRam());
                if(component != null) {
                    if(component.getName() != null) {
                        computerDTO.setRamName(component.getName());
                    }
                }
            }

            computerDTOS.add(computerDTO);
        }
        return computerDTOS;
    }

    /**
     * Returns dto
     * @param computer
     * @return
     */
    private ComputerDTO returnDto(Computer computer) {
        List<Computer> computers = new ArrayList<>();
        computers.add(computer);

        List<ComputerDTO> computerDTO = convertEntityToDTO(computers);

        if(computerDTO.isEmpty()) {
            throw new RuntimeException("Error converting entity!");
        }

        return computerDTO.get(0);
    }

    /**
     * Returns true if all parameters are correct
     * @param computer
     * @return
     */
    private boolean validateComputer(Computer computer) {
        if(computer.getGpu() != 0) {
            if(!checkComponentType("GPU", computer.getGpu())){
                throw new RuntimeException("Component type is not correct!");
            }
        }
        if(computer.getCpu() != 0) {
            if(!checkComponentType("CPU", computer.getCpu())){
                throw new RuntimeException("Component type is not correct!");
            }
        }
        if(computer.getHdd() != 0) {
            if(!checkComponentType("HDD", computer.getHdd())) {
                throw new RuntimeException("Component type is not correct!");
            }
        }
        if(computer.getRam() != 0) {
            if(!checkComponentType("RAM", computer.getRam())){
                throw new RuntimeException("Component type is not correct!");
            }
        }
        return true;
    }

    /**
     * Checks component type
     * If component has right type return true
     * @param componentId
     * @return
     */
    private boolean checkComponentType(String type, int componentId) {
        Component component = componentService.getComponent(componentId);
        if(component != null) {
            if(component.getComponenttype() != null) {
                if(component.getComponenttype().equals(type)){
                    return true;
                }
            }
        }
        return false;
    }
}
