package pc.desire.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pc.desire.app.helper.OibValidator;
import pc.desire.app.model.CompUser;
import pc.desire.app.model.User;
import pc.desire.app.service.CompUserService;
import pc.desire.app.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    CompUserService compUserService;

    @GetMapping("/getAll")
    public List<User> getAllUsers() {
        return userService.getAll();
    }

    @GetMapping("/getById/{id}")
    public User getUserById(@PathVariable final Integer id) {
        Optional<User> optional = userService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No User found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public User getUserByName(@PathVariable final String name) {
        return userService.getUserByName(name);
    }

    @GetMapping("/getBySurname/{surname}")
    public User getUserBySurname(@PathVariable final String surname) {
        return userService.getUserBySurname(surname);
    }

    @GetMapping("/getByOib/{oib}")
    public User getUserByOib(@PathVariable final String oib) {
        if(!OibValidator.checkOIB(oib)) {
            throw new RuntimeException("Oib is not valid!");
        }
        return userService.getUserByOib(oib);
    }

    @PostMapping("/createUser")
    public void createUser(@RequestBody User user) {
        if(user.getName() == null || user.getSurname() == null || user.getAddress() == null || user.getOib() == null) {
            throw new RuntimeException("User does not have all arguments!");
        } else if(userService.getUserByOib(user.getOib()) != null) {
            throw new RuntimeException("User already exists!");
        }
        if(!OibValidator.checkOIB(user.getOib())) {
            throw new RuntimeException("Oib is not valid!");
        }

        userService.createUser(user);
    }

    @PutMapping("/updateUser/{id}")
    public void updateUser(@RequestBody User user, @PathVariable int id) {

        Optional<User> optional = userService.findById(id);
        User currentUser = optional.get();

        if (currentUser != null) {
            if(user.getName() != null)
                currentUser.setName(user.getName());
            if(user.getSurname() != null) {
                currentUser.setSurname(user.getSurname());
            }
            if(user.getAddress() != null) {
                currentUser.setAddress(user.getAddress());
            }
            if(user.getOib() != null) {
                if(OibValidator.checkOIB(user.getOib())) {
                    currentUser.setOib(user.getOib());
                } else {
                    throw new RuntimeException("Oib is not valid!");
                }
            }

            userService.createUser(currentUser);
        } else {
            throw new RuntimeException("User does not exist!");
        }
    }

    @GetMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable final Integer id) {
        User user = userService.getUser(id);

        if(user != null) {
            userService.deleteUser(user);
        }
    }

    @GetMapping("/search/{name}")
    public List<User> findAll(@RequestParam Optional<String> name) {
        return userService.findAll(name.orElse("_"));
    }

    @GetMapping("/getAllInterestedUsersOnPc/{pcid}")
    public List<User> findAllInterestedUsers(@PathVariable final Integer pcid) {
        List<CompUser> compUsers = compUserService.getCompUserByPcId(pcid);

        List<Integer> userIds = compUsers.
                                stream().
                                map(CompUser::getUserid).
                                collect(Collectors.toList());

        List<User> users = userService.getUsersByUserIds(userIds);
        return users;
    }

}
