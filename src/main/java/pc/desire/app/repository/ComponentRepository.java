package pc.desire.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pc.desire.app.model.Component;

import java.util.List;

public interface ComponentRepository extends JpaRepository<Component, Integer> {

    Component findByName(String name);

    List<Component> findAllByComponenttype(String componenttype);
}
