package pc.desire.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pc.desire.app.model.Computer;

import java.util.List;

public interface ComputerRepository extends JpaRepository<Computer, Integer> {

    Computer findByName(String name);

    List<Computer> findByIdIn(List<Integer> computerIds);
}
