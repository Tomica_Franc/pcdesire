package pc.desire.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pc.desire.app.model.CompUser;

import java.util.List;

public interface CompUserRepository extends JpaRepository<CompUser, Integer> {

    List<CompUser> findAllByUserid(Integer userId);

    List<CompUser> findAllByComputerid(Integer pcId);

    CompUser findByUseridAndComputerid(Integer userid, Integer computerid);
}
