package pc.desire.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pc.desire.app.model.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByName(String name);

    User findBySurname(String surname);

    User findByOib(String oib);

    @Query("select u from User u where name like %?1% or surname like %?1%")
    List<User> getAllByName(String name);

    List<User> findByIdIn(List<Integer> userIds);
}
